# Introduction

This is a simple Scala project used as an initial learning exercise.

It creates a simple HTML page with links to search queries. The queries
are for recent news on technology subjects and technology markets.
