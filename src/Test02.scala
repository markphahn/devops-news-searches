/* testing a scala program */

object Test02 {

  val searchStrings = Array(
    "devops", "cloud computing", "devops market", "cloud computing market")

  val newsSites = Array(
    "cnet.com", "computerworld.com", "infoworld.com", "itworld.com", "informationweek.com",
    "techtarget.com", "eweek.com", "techcrunch.com", "technologyreview.com", "geekwire.com",
    "theregister.co.uk", "cnet.com", "gartner.com", "idg.com")

  val searchEngines = Array("https://www.google.com/search?q=")

  val searches = Map(
    "News Past Week" -> "&tbm=nws&tbs=qdr:w",
    "News Past Month" -> "&tbm=nws&tbs=qdr:m")

  def main(args: Array[String]) {
    println("Hello Scala !!")

    var home = sys.env("HOME")
    println(home)
    var filename = "x";
    var f = new java.io.File(home, "Documents/news_searches.html");
    println(f.getAbsolutePath)
    var w = new java.io.PrintStream(f);

    w.printf("<html>\n");
    w.printf("<title>DevOps News Searches</title>\n");
    w.printf("<body>\n");

    for ((title, searchOptions) <- searches) buildTopic(title, searchOptions, w)

    w.printf("</body></html>\n");
    w.flush();
    w.close();
  }

  def buildTopic(title: String, searchOptions: String, w: java.io.PrintStream) {
    printf("\n%s\n", title)
    w.printf("<h2>%s</h2>\n", title)
    for (searchTerm <- searchStrings) {
      for (newsSite <- newsSites) buildTermSearch(searchTerm, newsSite)
      w.printf("<br/>\n");
    }

    def buildTermSearch(term: String, site: String) {
      for (engine <- searchEngines) href(engine, term.replace(" ", "+"));

      def href(engine: String, searchString: String) {
        var href = engine + searchString + "+site:" + site + searchOptions;
        println(href);

        w.printf("  <a href='%s'>%s %s</a>\n", href, site, term);
        w.printf("<br/>\n");
      }
    }
  }
}
